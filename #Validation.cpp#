#include <stdio.h>
#include <iostream>
/*
Julia Oppenheim and Natalie Martinez
joppenh8, joppenh8@jhu.edu
nmarti46, nmarti46@jhu.edu
May 5, 2017
Final Project (Intermediate)
 */

#include <string.h>
#include <cstring>
#include <cstdlib>
#include <cstdbool>
#include <cassert>
#include "Game.h"
#include "Prompts.h"
#include "Chess.h"

int Board::isCapture(Position start, Position end) const {
    if (this->getPiece(end) != NULL) {
        if (this->getPiece(end)->owner() != this->getPiece(start)->owner()) {
            return MOVE_CAPTURE;
        }
    }
    return -11;
}

int King::validMove(Position start, Position end,
                    const Board& board) const {

    if (end.x == start.x && end.y == start.y - 1) { //one spot to left
        return SUCCESS;
    } else if (end.x == start.x && end.y == start.y + 1) { //one spot to right
        return SUCCESS;
    } else if (end.y == start.y && end.x == start.x + 1) { //one spot forward
        return SUCCESS;
    } else if (end.y == start.y && end.x == start.x - 1) { //one spot backwards
        return SUCCESS;
    } else if (end.y == start.y - 1 && end.x == start.x - 1) { //one spot backwards left diagonal
        return SUCCESS;
    } else if (end.y == start.y + 1 && end.x == start.x - 1) { //one spot backwards right diagonal
        return SUCCESS;
    } else if (end. y == start.y + 1 && end.x == start.x + 1) { //one spot forward right diagonal
        return SUCCESS;
    } else if (end.y == start.y - 1 && end.x == start.x + 1) { //forward left diagonal
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;
}

int Knight::validMove(Position start, Position end, const Board& board) const {

    Prompts p;
    if ((end.x == start.x - 2 || end.x == start.x + 2) && (end.y == start.y + 1
            || end.y == start.y - 1)) { //wide Ls
        return SUCCESS;
    } else if ((end.y == start.y - 2 || end.y == start.y + 2) && (end.x == start.x + 1
               || end.x == start.x - 1)) { //tall Ls
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;
}

int Rook::validMove(Position start, Position end, const Board& board) const {

    Prompts p;
    if (end.x == start.x && end.y != start.y) { //move vert
        return SUCCESS;
    } else if (end.y == start.y && end.x != start.x) { //move horiz
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;
}

int Bishop::validMove(Position start, Position end, const Board& board) const {

    Prompts p;
    if (abs(end.y - start.y) == abs (end.x - start.x)) {
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;
}

int Queen::validMove(Position start, Position end, const Board& board) const {

    Prompts p;
    if ((end.y - start.y) / (end.x - start.x) == 1) {
        return SUCCESS;
    } else if (end.x == start.x && end.y != start.y) {
        return SUCCESS;
    } else if (end.y == start.y && end.x != start.x) {
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;
}

int Pawn::validMove(Position start, Position end, const Board& board) const {
    if (board.getPiece(start)->owner() == 0) { //white pawn
        if (end.y == start.y + 2 && board.getPiece(end) == NULL && start.y == 1) {
                return SUCCESS;
        } else if (end.y == start.y + 1 && board.getPiece(end) == NULL) {
                return SUCCESS;
        } else if (end.y == start.y + 1 && end.x == start.x + 1) {
            if (board.getPiece(end) != NULL && board.getPiece(end)->owner() == 1) {
                board.isCapture(start, end);
                return SUCCESS;
            }
        } else if (end.y == start.y + 1 && end.x == start.x - 1) {
            if (board.getPiece(end) != NULL && board.getPiece(end)->owner() == 1) {
                board.isCapture(start, end);
                return SUCCESS;
            }
        }
    else if (board.getPiece(start)->owner() == 1) { //black pawn
        if (end.y == start.y - 2 && board.getPiece(end) == NULL && start.y == 6) {
                return SUCCESS;
        } else if (end.y == start.y - 1 && board.getPiece(end) == NULL) {
                return SUCCESS;
        } else if (end.y == start.y - 1 && end.x == start.x + 1) {
            if (board.getPiece(end) != NULL && board.getPiece(end)->owner() == 0) {
                board.isCapture(start, end);
                return SUCCESS;
            }
        } else if (end.y == start.y - 1 && end.x == start.x - 1) {
            if (board.getPiece(end) != NULL && board.getPiece(end)->owner() == 0) {
                board.isCapture(start, end);
                return SUCCESS;
            }
        }
    }
    return MOVE_ERROR_ILLEGAL;
    }
}

int Piece::diagonalBlock(Position start, Position end, const Board& board) const  {
    Position blocked;
    blocked.x = start.x;
    blocked.y = start.y;

    if (board.getPiece(end)->owner() == board.getPiece(start)->owner()) {
        return MOVE_ERROR_BLOCKED;
    } else if ((end.y - start.y) / (end.x - start.x) == 1) {
        if (start.y > end.y) { //forward right  diagonal
            while (blocked.y != end.y) {
                if (board.getPiece(blocked) != NULL) {
                    return MOVE_ERROR_BLOCKED;
                }
                blocked.x++;
                blocked.y++;
            }
        } else if (start.y < end.y) {
            while (blocked.y != end.y) {
                if (board.getPiece(blocked) != NULL) {
                    return MOVE_ERROR_BLOCKED;
                }
                blocked.x --;
                blocked.y--;
            }
        }
    } else if ((end.y - start.y) / (end.x - start.x) == -1) {
        if (start.y > end.y) {
            while (blocked.y != end.y) {
                if (board.getPiece(blocked) != NULL) {
                    return MOVE_ERROR_BLOCKED;
                }
                blocked.x++;
                blocked.y--;
            }
        } else if (start.y < end.y) {
            while (blocked.y != end.y) {
                if (board.getPiece(blocked) != NULL) {
                    return MOVE_ERROR_BLOCKED;
                }
                blocked.x--;
                blocked.y++;
            }
        }
    }
    return -11;
}

int Piece::straightBlock(Position start, Position end, const Board& board) const {
    Position block;
    if (end.x == start.x && end.y != start.y) {
        block.x = start.x;
        for (int i = start.y; i < end.y; i += 8) {
            block.y = i;
            if (board.getPiece(block) != NULL) {
                return MOVE_ERROR_BLOCKED;
            }
        }
    }

    else if (end.y == start.y && end.x != start.x) {
        block.y = start.y;
        for (int i = start.x; i < end.x; i++) {
            block.x = i;
            if (board.getPiece(block) != NULL) {
                return MOVE_ERROR_BLOCKED;
            }
        }
    }

    else if (board.getPiece(end) == board.getPiece(start)) {
        return MOVE_ERROR_BLOCKED;
    }
    return -11;
}

//for king: can't be blocked can only be capture::call isCapture when call
int King::blocked(Position start, Position end, const Board& board) const  {
    if (board.getPiece(end)->owner() == board.getPiece(start)->owner()) {
        return MOVE_ERROR_BLOCKED;
    }
    return -11;
}

int Knight::blocked(Position start, Position end, const Board& board) const {
    return -11;
}

int Rook::blocked(Position start, Position end, const Board& board) const {
    int answer = straightBlock(start, end, board);
    return answer;

}

int Bishop::blocked(Position start, Position end, Board& board) const {

    int answer = diagonalBlock(start, end, board);
    return answer;
}

int Queen::blocked(Position start, Position end, const Board& board) const  {
    if (diagonalBlock(start, end, board) == -5 || straightBlock(start, end, board) == -5) {
        return MOVE_ERROR_BLOCKED;
    }
    return -11;
}

int ChessGame::findKing(Player pl) {
  int counter = 0;
  while (m_pieces[counter] -> owner() != pl && m_pieces[counter]->id() != 5){
    counter++;
  }
  return counter; 
}

Position ChessGame::convertPosition(int index, char coordinate[2]) {
  Position p;
  getCoor(index, coordinate); 
  p.x = coordinate[0] - 97;
  p.y = coordinate[1] - 49;
  return p; 
}

bool ChessGame::checkScenario(Player pl) {
  int result, block;
  Piece* aggressor;
  char ch[2], ch2[2];
  Position pAg, pKg; 
  int kingSpot = findKing(pl);
  pKg = convertPosition(kingSpot, ch);

  for (int i = 0; i < 64; i++) {
    if (m_pieces[i] -> owner() == pl) {
      pAg = convertPosition(i, ch);
      aggressor = getPiece(pAg);
      result = aggressor->validMove(pAg, pKg, *this);
      block = aggressor->blocked(pAg, pKg, *this);
      if (result == 1 && block == -5) {
	return true; 
      }
    }
  }
  return false; 
}

//do this before actually making a move 
int ChessGame::exposeCheck(Player pl) {
   if (checkScenario(pl)) {
    return MOVE_ERROR_CANT_EXPOSE_CHECK; 
    }
  return -11; 
}

//do this after a move is made
int ChessGame::check(Player pl) {
  if (checkScenario(pl)) {
    return MOVE_CHECK; 
    }
  return -11;
}

int Pawn::blocked(Position start, Position end, const Board& board) const {
    return -11;
}
