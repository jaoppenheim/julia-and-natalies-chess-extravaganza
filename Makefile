CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g -w

chess: Chess.o Board.o Validation.o main.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o Validation.o main.o -o chess
Board.o: Game.h
Validation.o: Game.h Prompts.h Chess.h
Chess.o: Game.h Chess.h Prompts.h 
main.o: Game.h Chess.h Prompts.h main.cpp

clean:
	rm *.o chess

