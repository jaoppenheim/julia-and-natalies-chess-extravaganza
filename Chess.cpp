#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <ctype.h>
#include <fstream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "terminal.h"

using std::vector;
using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::ofstream;
using std::ifstream;

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {

    unsigned int indexS = index(start);
    unsigned int indexE = index(end);
    Piece* piece = m_pieces.at(indexS);
    int valid = piece->validMove(start, end, *this);
    int block = piece->blocked(start, end, *this);
    int cap = Board::isCapture(start, end);

    if (valid > -1) {
      if (block == -11 ) {
            m_pieces[indexE] = m_pieces[indexS];
            m_pieces[indexS] = NULL;
            if (cap == 3) {
                return 0;
            }
            return 1;
        } else {
            return 2;
        }
        return 3;
    }
}
// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}
void Board::displayBoard() {

    int counter = 0;
    int pattern = 0;
    int ok;
    Player see;
    for (size_t i = 0; i < 8; i++) {
        for (size_t j = 0; j < 8; j++) {
            if (pattern % 2 == 0) {
                Terminal::colorBg(Terminal::WHITE);
            } else {
                Terminal::colorBg(Terminal::CYAN);
            }
            Terminal::colorFg(false, Terminal::BLACK);
            if (!(this->m_pieces.at(counter))) {
                cout << "  ";
            } else {
                switch (this->m_pieces.at(counter)->id()) {
                case 1:
                    if (this->m_pieces.at(counter)->owner() == WHITE) {
                        cout << "\u2656 ";
                        break;
                    } else {
                        cout << "\u265C ";
                        break;
                    }
                case 2:
                    if (this->m_pieces.at(counter)->owner() == WHITE) {
                        cout << "\u2658 ";
                        break;
                    } else {
                        cout << "\u265E ";
                        break;
                    }
                case 3:
                    if (this->m_pieces.at(counter)->owner() == WHITE) {
                        cout << "\u2657 ";
                        break;
                    } else {
                        cout << "\u265D ";
                        break;
                    }
                case 4:
                    if (this->m_pieces.at(counter)->owner() == WHITE) {
                        cout << "\u2655 ";
                        break;
                    } else {
                        cout << "\u265B ";
                        break;
                    }
                case 5:
                    if (this->m_pieces.at(counter)->owner() == WHITE) {
                        cout << "\u2654 ";
                        break;
                    } else {
                        cout << "\u265A ";
                        break;
                    }
                case 0:
                    if (this->m_pieces.at(counter)->owner() == WHITE) {
                        cout << "\u2659 ";
                        break;
                    } else {
                        cout << "\u265F ";
                        break;
                    }
                }
            }
            counter++;
            pattern++;
        }
        pattern++;
        Terminal::set_default();
        cout << endl;
    }
}

int Board::validEntry(string entry) {

    if (entry.length() != 5) {
        return 1;//fails based on length of string
    }

    else if (entry.at(2) != ' ') {
        return 1;//fails because there is no space
    }

    else if (isalpha(entry.at(0)) == 0 && isalpha(entry.at(3)) == 0 &&
             isalpha(entry.at(1)) != 0 && isalpha(entry.at(4)) != 0) {
        return 1;//fails because incorrect placement for letters and numbers
    } else {
        return 0;
    }

}

void Board::saveGame(string filename, int turns) {

    char coo[2];
    ofstream outputFile;
    outputFile.open(filename, ofstream::out);

    outputFile << "chess" << endl;//need to modify to take any game name
    outputFile << turns << endl;

    int counter = 0;

    while (counter <= (m_height * m_width) - 1) {
        if (this->m_pieces.at(counter)) {
            outputFile << m_pieces.at(counter)->owner() << " ";
            getCoor(counter, coo);
            outputFile << coo[0] << coo[1] << " ";
            outputFile << m_pieces.at(counter)->id() << endl;
            counter++;
        } else if (!(this->m_pieces.at(counter))) {
            counter++;
        }
    }
}

void Board::getCoor(int index, char coordinate[]) {

    int letter = index % 8 + 97;
    int number = index % 8 + 49 + (index / 8) - letter + 97;
    coordinate[0] = letter;
    coordinate[1] = number;
}

int Board::loadSave(string file) {

    string game, line;
    int turns;
    unsigned char x;
    unsigned int y, person, piece, tempIndex;
    Position temp;
    bool yes;
    Player tPlayer;

    ifstream inputFile;
    inputFile.open(file, ifstream::in);

    inputFile >> game;
    if (game == "chess") {
        inputFile >> m_turn;
        while (inputFile >> person >> x >> y >> piece) {
            temp.x = x - 97;
            temp.y = y - 1;
            tempIndex = index(temp);
            if (person == 0) {
                tPlayer = WHITE;
            } else {
                tPlayer = BLACK;
            }
            yes = initPiece(piece, tPlayer, temp);
        }
        return 0;
    } else {
        return 1;
    }
}

void ChessGame::run() {

    Prompts p;
    Position start, end;
    Player turn;
    char choice;
    string filename, option, take;
    int type, spotS, spotE, exists, check, move;
    unsigned int indexS;
    int quit = 0;
    int boardToggle = 0;
    int endLoop = 0;


    //Initial menu options
    while (!endLoop) {
        p.menu();
        choice = getchar();
        switch (choice) {
        case '1':
            setupBoard();
            endLoop = 1;
            break;
        case '2':
            p.loadGame();
            cin >> filename;
            check = loadSave(filename);
            if (check) {
                p.loadFailure();
                exit(1);
            }
            endLoop = 1;
            break;
        default:
            getline(cin, option);
            break;
        }
    }

    choice = getchar();//gets newline from 1 or 2

    //Gameplay options
    while (!quit) {//operate loop until user quits
        turn = playerTurn();
        p.playerPrompt(turn, (m_turn + 1) / 2);
        getline(cin, option);//gets player input
        for (int i = 0; i < option.length(); i++) {//formats to lowercase
            if (isalpha(option[i]) != 0) {
                option[i] = tolower(option[i]);
            }
        }
        type = validEntry(option);//checks if is move
        if (type == 0) {//options if entry is in move format
            start.x = option.at(0) - 97;
            start.y = option.at(1) - 49;
            end.x = option.at(3) - 97;
            end.y = option.at(4) - 49;
            if (validPosition(start) && validPosition(end)) { //checks if move is on board
                indexS = index(start);
                if ((this->getPiece(start)) == NULL) { //checks if move is null
                    p.noPiece();
                } else if (getPiece(start)->owner() != turn) {
                    p.noPiece();
                } else {
                    move = makeMove(start, end);
                    if (move == 0) {
                        p.capture(turn);
                        if (boardToggle == 1) {
                            displayBoard();
                        }
                        m_turn++;
                    } else if (move == 2) {
                        p.blocked();
                    } else if (move == 3) {
                        p.illegalMove();
                    } else if (move == 1) {
                        if (boardToggle == 1) {
                            displayBoard();
                        }
                        m_turn++;
                    }
                }
            } else if (!(validPosition(start) && validPosition(end))) { //off the board
                p.outOfBounds();
            }
        } else {
            if (option == "board") {
                if (!boardToggle) {
                    boardToggle = 1;
                    displayBoard();
                } else {
                    boardToggle = 0;
                }
            } else if (option == "save") {
                p.saveGame();
                cin >> filename;
                saveGame(filename, m_turn);
                getline(cin, take);
            } else if (option == "forfeit") {
                if (turn == WHITE) {
                    p.win(BLACK, ((m_turn + 1) / 2));
                } else {
                    p.win(WHITE, ((m_turn + 1) / 2) + 1);
                }
                p.gameOver();
                quit = 1;
            } else if (option == "q") {
                quit = 1;
            } else {
                p.parseError();
            }
        }
    }
}

